��             +         �  ,   �  -   �     $  /   1     a     v     �     �     �  >   �  >         ?      F     g  +   |  ,   �     �     �     �            9        S     Y     t  '   y     �     �      �     �     �  �  �  +   �  .   �     �  2   �          +  	   9     C     R  3   d  B   �     �      �     	  '   	  )   A	     k	     t	     �	     �	      �	  A   �	     
      
  	   =
  #   G
     k
     �
  *   �
     �
     �
                 	                                                                                                                           
    %(count)d announcement deleted successfully. %(count)d announcements deleted successfully. Announcement Announcement "%(object)s" deleted successfully. Announcement created Announcement edited Announcements Create announcement Create announcements Date and time after which this announcement will be displayed. Date and time until when this announcement is to be displayed. Delete Delete announcement: %(object)s. Delete announcements Delete the %(count)d selected announcement. Delete the %(count)d selected announcements. Edit Edit announcement: %s Edit announcements Enabled End date time Error deleting announcement "%(instance)s"; %(exception)s Label No announcements available None Short description of this announcement. Start date time Text The actual text to be displayed. URL View announcements Project-Id-Version: Mayan EDMS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-02 18:58+0000
Last-Translator: Harald Ersch, 2024
Language-Team: Romanian (Romania) (https://app.transifex.com/rosarior/teams/13584/ro_RO/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ro_RO
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 Anunțul %(count)d a fost șters cu succes. %(count)d anunțuri au fost șterse cu succes. Anunţ Anunțul „%(object)s” a fost șters cu succes. Anunț creat Anunț editat Anunțuri Creați anunț Creați anunțuri Data și ora după care va fi afișat acest anunț. Data și ora până la care urmează să fie afișat acest anunț. Șterge Ștergeți anunțul: %(object)s. Ștergeți anunțuri Ștergeți anunțul selectat %(count)d. Ștergeți %(count)d anunțuri selectate. Editați Editați anunțul: %s Editați anunțuri Activat Marcaj temporal al sfârșitului Eroare la ștergerea anunțului „%(instance)s”; %(exception)s Conținut etichetă Nu există anunțuri disponibile Nici unul Scurtă descriere a acestui anunț. Marcaj temporal al începutului Text Textul real care urmează să fie afișat. URL Vizualizați anunțuri 
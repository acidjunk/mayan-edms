��    5      �  G   l      �     �  	   �  +   �  	   �     �     �     �     �          !     .     5     D     U     j     �     �     �  2   �     �       !        >     C     P     _     g  *   n  (   �     �     �     �               "     0     =     D  +   P  ,   |      �     �     �  )   �             	      "   *  &   M  "   t  $   �     �  �  �     a
     }
  1   �
  	   �
     �
     �
     �
     �
               $     ,     ?     S  0   r      �  1   �     �  1   
     <     P  2   ^     �     �     �     �     �  %   �  5   �     )  !   F     h     �     �     �     �     �     �  9   �  (      (   )  	   R     \  )   i     �     �     �  '   �  1   �  '   1  +   Y  	   �            &               $                                                 -         "   !         '                            .      3             )           5               *               %   1       (   4      /   
      #   2   	   ,      0   +    All document types All users Allow quick disable or enable of the quota. Arguments Backend Backend data Backend path Create a "%s" quota Create a quota Create quota Delete Delete a quota Delete quota: %s Document count limit Document count quota exceeded. Document size limit Document size quota exceeded. Document types Document types to which the quota will be applied. Documents limit Does not apply Driver used for this quota entry. Edit Edit a quota Edit quota: %s Enabled Groups Groups to which the quota will be applied. Maximum document size in megabytes (MB). Maximum number of documents. New quota backend selection No quotas available Null backend Quota Quota created Quota edited Quotas Quotas list Quotas restrict usage of system resources.  The dotted Python path to the backend class. The quota driver for this entry. Usage Users Users to which the quota will be applied. View a quota all document types all users document count: %(document_count)s document size: %(formatted_file_size)s document types: %(document_types)s groups: %(groups)s, users: %(users)s none Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-02 18:58+0000
Last-Translator: Harald Ersch, 2024
Language-Team: Romanian (Romania) (https://app.transifex.com/rosarior/teams/13584/ro_RO/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ro_RO
Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));
 Toate tipurile de documente Toți utilizatorii Permite dezactivarea rapidă sau activarea cotei. Argumente Backend Date backend Calea de backend Creați o cotă "%s" Creați o cotă Creați cota Șterge Ștergeți o cotă Ștergeți cota: %s Limita numărului de documente Cota numărului de documente a fost depășită. Dimensiunea documentului limită Cota dimensiunii documentului a fost depășită. Tipuri de documente Tipuri de documente cărora li se va aplica cota. Limita documentelor Nu se aplică Pilotul utilizat pentru această intrare de cotă. Editați Editați o cotă Modifica cota: %s Activat Grupuri Grupuri cărora li se va aplica cota. Dimensiunea maximă a documentului în megabyte (MB). Numărul maxim de documente. Noua selecție de backend de cote Nu există cote disponibile Backend vid Cotă Cotă creată Cotă editată Cote Lista cotelor Cotele restricționează utilizarea resurselor de sistem. Calea Python punctată la clasa backend. Pilotul cotelor pentru această intrare. utilizare Utilizatorii Utilizatori cărora li se va aplica cota. Vizualizați o cotă toate tipurile de documente toți utilizatorii număr de documente: %(document_count)s dimensiunea documentului: %(formatted_file_size)s tipuri de documente: %(document_types)s grupuri: %(groups)s, utilizatori: %(users)s nici unul 
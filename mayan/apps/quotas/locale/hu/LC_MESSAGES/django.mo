��    5      �  G   l      �     �  	   �  +   �  	   �     �     �     �     �          !     .     5     D     U     j     �     �     �  2   �     �       !        >     C     P     _     g  *   n  (   �     �     �     �               "     0     =     D  +   P  ,   |      �     �     �  )   �             	      "   *  &   M  "   t  $   �     �  j  �     ,
     D
  /   Y
     �
     �
     �
      �
     �
     �
          (     1     B     W  $   o     �  '   �     �  <   �     %     7  7   H     �     �     �     �  	   �  3   �  2        ;  '   [     �     �     �     �     �     �     �  ?      7   @  2   x  
   �     �  5   �     �          (  '   =  +   e  &   �  1   �     �            &               $                                                 -         "   !         '                            .      3             )           5               *               %   1       (   4      /   
      #   2   	   ,      0   +    All document types All users Allow quick disable or enable of the quota. Arguments Backend Backend data Backend path Create a "%s" quota Create a quota Create quota Delete Delete a quota Delete quota: %s Document count limit Document count quota exceeded. Document size limit Document size quota exceeded. Document types Document types to which the quota will be applied. Documents limit Does not apply Driver used for this quota entry. Edit Edit a quota Edit quota: %s Enabled Groups Groups to which the quota will be applied. Maximum document size in megabytes (MB). Maximum number of documents. New quota backend selection No quotas available Null backend Quota Quota created Quota edited Quotas Quotas list Quotas restrict usage of system resources.  The dotted Python path to the backend class. The quota driver for this entry. Usage Users Users to which the quota will be applied. View a quota all document types all users document count: %(document_count)s document size: %(formatted_file_size)s document types: %(document_types)s groups: %(groups)s, users: %(users)s none Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-02 18:58+0000
Last-Translator: Csaba Tarjányi, 2024
Language-Team: Hungarian (https://app.transifex.com/rosarior/teams/13584/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Minden dokumentumtípus Minden felhasználó A kvóta gyors letiltása vagy engedélyezése. Argumentumok Háttérrendszer Háttérrendszerbeli adatok Háttérrendszer elérési útja "%s" kvóta létrehozása Kvóta létrehozása Kvóta létrehozása Törlés Kvóta törlése Kvóta törlése: %s Dokumentumszám korlát A dokumentumszám-kvóta túllépve. Dokumentum méretkorlát Túllépte a dokumentumméret-kvótát. Dokumentumtípusok Dokumentumtípusok, amelyekre a kvótát alkalmazni fogják. Dokumentumkorlát Nincs alkalmazva Ehhez a kvótabejegyzéshez használt illesztőprogram. Szerkesztés Kvóta szerkesztése Kvóta szerkesztése: %s Engedélyezett Csoportok Csoportok, amelyekre a kvótát alkalmazni fogják. A dokumentum maximális mérete megabájtban (MB). Dokumentumok maximális száma. Új kvóta háttérrendszer választás Nem áll rendelkezésre kvóta Null háttérrendszer Kvóta Kvóta létrehozva Kvóta szerkesztve Kvóták Kvóták listája A kvóták korlátozzák a rendszererőforrások használatát. A pontozott Python elérési út a háttérosztályhoz. A kvóta illesztőprogramja ehhez a bejegyzéshez. Használat Felhasználók Felhasználók, akikre a kvótát alkalmazni fogják. Kvóta megtekintése minden dokumentumtípus minden felhasználó dokumentumok száma: %(document_count)s dokumentum mérete: %(formatted_file_size)s dokumentumtípusok: %(document_types)s csoportok: %(groups)s , felhasználók: %(users)s nincs 